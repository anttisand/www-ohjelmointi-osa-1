<?php

class QueryBuilder {

	protected $pdo;

	// Dependency injection pattern
	public function __construct(PDO $pdo) {
		$this->pdo = $pdo;
	}

	public function getAll($table, $asClass) {
		$statement = $this->pdo->prepare("select * from {$table}");
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_CLASS, "$asClass");
	}
}