<?php
//core/bootstrap.php
$config = require 'config.php';
require "database/connection.php";
require "database/QueryBuilder.php";
require "functions/functions.php";

$db = new QueryBuilder(
	Connection::make($config['database'])
);